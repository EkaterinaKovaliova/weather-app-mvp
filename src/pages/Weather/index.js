import React from 'react';

import WeatherAppDescription from 'modules/Weather/components/WeatherAppDescription';
import WeatherForm from 'modules/Weather/components/WeatherForm';

import { WeatherPageWrapper } from './WeatherStyles';

const Weather = () => {
  return (
    <WeatherPageWrapper>
      <WeatherAppDescription />
      <WeatherForm />
    </WeatherPageWrapper>
  );
};

export default Weather;
