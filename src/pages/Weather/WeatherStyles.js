import styled from 'styled-components';

import { spaces, colors, breakPoints } from 'styles';

export const WeatherPageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  flex: 1;
  padding: ${spaces.SPACE_20};
  background-color: ${colors.PRIMARY_DARK};

  @media (min-width: ${breakPoints.BREAKPOINT_1024}) {
    flex-direction: row;
    align-items: flex-start;
    padding-left: ${spaces.SPACE_100};
  }
`;
