import styled from 'styled-components';

import { Logo } from 'assets';

import { spaces, breakPoints } from 'styles';

export const WeatherDescriptionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: fit-content;
  height: fit-content;
  margin-bottom: ${spaces.SPACE_60};

  @media (min-width: ${breakPoints.BREAKPOINT_600}) {
    text-align: center;
  }

  @media (min-width: ${breakPoints.BREAKPOINT_1024}) {
    margin-bottom: 0;
    text-align: left;
  }
`;

export const LogoImg = styled.img.attrs({
  src: Logo,
  alt: 'logo',
})`
  width: 100px;
  height: 100px;
`;

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: ${breakPoints.BREAKPOINT_1024}) {
    align-items: flex-start;
  }
`;

export const DescriptionWrapper = styled.div`
  min-width: 250px;
  max-width: 400px;
  margin-top: ${spaces.SPACE_30};

  @media (min-width: ${breakPoints.BREAKPOINT_600}) {
    max-width: 600px;
    margin-top: ${spaces.SPACE_50};
  }
  @media (min-width: ${breakPoints.BREAKPOINT_1024}) {
    max-width: 500px;
  }
`;
