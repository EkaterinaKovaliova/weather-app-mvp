import React from 'react';

import { Text } from 'components';

import {
  WeatherDescriptionWrapper,
  LogoImg,
  InfoContainer,
  DescriptionWrapper,
} from './WeatherAppDescriptionStyles';

function WeatherAppDescription() {
  return (
    <WeatherDescriptionWrapper>
      <LogoImg />
      <InfoContainer>
        <Text pageTitle>Weather App</Text>
        <DescriptionWrapper>
          <Text pageDescription>
            This is a simple application that allows you to get weather data for any city in the
            world
          </Text>
        </DescriptionWrapper>
      </InfoContainer>
    </WeatherDescriptionWrapper>
  );
}

export default WeatherAppDescription;
