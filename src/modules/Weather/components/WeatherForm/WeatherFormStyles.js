import styled from 'styled-components';

import { colors, spaces, breakPoints } from 'styles';

export const WeatherFormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: fit-content;
  height: fit-content;
  background-color: ${colors.PRIMARY_DARK};

  @media (min-width: ${breakPoints.BREAKPOINT_1024}) {
    margin-top: ${spaces.SPACE_100};
    margin-left: ${spaces.SPACE_250};
  }
`;

export const WeatherFormContainer = styled.form`
  display: flex;
  flex-direction: column;
  min-width: 250px;
  max-width: 400px;
  min-height: 200px;
  max-height: fit-content;
  margin-bottom: ${spaces.SPACE_30};
  padding: ${spaces.SPACE_20};
  border: 1px solid ${colors.GREY};
  border-radius: 10px;
  background-color: ${colors.MAIN_WHITE};

  @media (min-width: ${breakPoints.BREAKPOINT_600}) {
    min-width: 350px;
  }
`;

export const InputContainer = styled.div`
  width: 100%;
  margin-top: ${spaces.SPACE_20};
`;

export const ButtonContainer = styled.div`
  width: 100%;
  height: 45px;

  :first-child {
    margin-right: ${spaces.SPACE_20};
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
`;

export const WeatherInfoWrapper = styled.div`
  opacity: ${({ $isAppear }) => ($isAppear ? '1' : '0')};
  min-width: 250px;
  max-width: 400px;
  min-height: 250px;
  max-height: fit-content;
  border: 1px solid ${colors.GREY};
  border-radius: 10px;
  background-color: ${colors.MAIN_WHITE};
  transition: 0.7s ease-in-out;

  @media (min-width: ${breakPoints.BREAKPOINT_600}) {
    min-width: 300px;
  }
`;

export const WeatherInfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${spaces.SPACE_20};
`;

export const ListContainer = styled.div`
  margin-top: ${spaces.SPACE_20};

  & > p {
    margin-top: ${spaces.SPACE_4};

    ::first-letter {
      font-weight: bold;
    }
  }
`;
