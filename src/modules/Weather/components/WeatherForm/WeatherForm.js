import React from 'react';
import { useFormik } from 'formik';
import { useSelector, useDispatch } from 'react-redux';

import { weatherSliceActionTypes } from 'modules/Weather/redux/weatherSlice';
import { Button, Text, TextField } from 'components';
import {
  weatherDataSelector,
  citySelector,
  isFetchingWeatherDataSelector,
  errorSelector,
} from 'modules/Weather/redux/selectors';

import { buttonConfig, formikConfig, getButtonName, cleanButtonName } from './weatherFormsData';
import {
  ButtonContainer,
  ButtonWrapper,
  InputContainer,
  ListContainer,
  WeatherFormWrapper,
  WeatherInfoContainer,
  WeatherFormContainer,
  WeatherInfoWrapper,
} from './WeatherFormStyles';

function WeatherForm() {
  const dispatch = useDispatch();

  const weatherData = useSelector(weatherDataSelector);
  const city = useSelector(citySelector);
  const isFetching = useSelector(isFetchingWeatherDataSelector);
  const errorMessage = useSelector(errorSelector);

  const formik = useFormik({
    ...formikConfig,
    onSubmit: (values) => {
      dispatch(weatherSliceActionTypes.getWeatherDataRequest(values.cityName));
    },
  });

  const cleanupHandler = () => {
    formik.handleReset();
    dispatch(weatherSliceActionTypes.cleanWeatherData());
  };

  const buttons = Object.keys(buttonConfig).map((buttonName) => (
    <ButtonContainer key={`weatherButtonName-${buttonName}`}>
      <Button
        disabled={isFetching}
        destruction={buttonName === cleanButtonName}
        isFetching={buttonName === getButtonName && isFetching}
        text={buttonName}
        onClickHandler={buttonName === cleanButtonName ? cleanupHandler : () => {}}
        type={buttonConfig[buttonName]}
      />
    </ButtonContainer>
  ));
  const weatherReceived = weatherData.length !== 0;

  const weatherInfoList = Object.keys(weatherData).map((element) => (
    <Text key={`weatherItem-${element}`} formDescription>
      {element} : {weatherData[element]}
    </Text>
  ));

  const weatherInfo = (
    <WeatherInfoWrapper $isAppear={weatherReceived}>
      <WeatherInfoContainer>
        {weatherReceived && <Text>Weather in {city}: </Text>}
        <ListContainer>{weatherInfoList}</ListContainer>
      </WeatherInfoContainer>
    </WeatherInfoWrapper>
  );

  return (
    <WeatherFormWrapper>
      <WeatherFormContainer onSubmit={formik.handleSubmit}>
        <Text formTitle>Weather Form</Text>
        <InputContainer>
          <TextField
            name="cityName"
            label="City name"
            value={formik.values.cityName}
            onChange={formik.handleChange}
            error={formik.errors.cityName || errorMessage}
          />
        </InputContainer>
        <ButtonWrapper>{buttons}</ButtonWrapper>
      </WeatherFormContainer>
      {weatherInfo}
    </WeatherFormWrapper>
  );
}

export default WeatherForm;
