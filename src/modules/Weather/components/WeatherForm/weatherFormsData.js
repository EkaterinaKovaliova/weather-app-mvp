import * as Yup from 'yup';

const validationSchema = Yup.object({
  cityName: Yup.string()
    .matches(/^[A-Za-z- ]+$/, 'Only Latin letters are allowed')
    .min(2, 'Must be 2 characters or more')
    .required('Required'),
});

export const formikConfig = {
  initialValues: {
    cityName: '',
  },
  validateOnChange: false,
  validationSchema: validationSchema,
  onSubmit: () => {},
};

export const getButtonName = 'GET';
export const cleanButtonName = 'CLEAN';

export const buttonConfig = { [getButtonName]: 'submit', [cleanButtonName]: 'button' };
