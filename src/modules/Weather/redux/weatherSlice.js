import { createSlice } from '@reduxjs/toolkit';

const initialState = { weatherData: [], city: '', isFetching: false, error: null };

const weatherSlice = createSlice({
  name: 'WEATHER',
  initialState: initialState,
  reducers: {
    getWeatherDataSuccess: (state, { payload }) => {
      const { weatherData, city } = payload;

      state.weatherData = weatherData;
      state.city = city;
      state.isFetching = false;
    },
    getWeatherDataError: (state, { payload }) => {
      state.isFetching = false;
      state.error = payload;
      state.weatherData = null;
    },
    cleanWeatherData: (state) => initialState,
    getWeatherDataRequest: (state) => {
      state.isFetching = true;
      state.error = null;
    },
  },
});

export const weatherSliceActionTypes = weatherSlice.actions;

export default weatherSlice.reducer;
