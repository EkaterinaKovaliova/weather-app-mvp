import { put, all, takeLatest } from 'redux-saga/effects';

import { getWeather } from 'services/restServiceAPI';
import { capitalize, requestWrapper } from 'utils';

import { weatherSliceActionTypes } from './weatherSlice';
import { cityNormalizer, weatherDataNormalizer } from './normalizer';

function* getWeatherData({ payload }) {
  const { response: getWeatherResponse, error: getWeatherError } = yield requestWrapper(
    getWeather,
    payload
  );
  if (getWeatherResponse) {
    const weatherData = weatherDataNormalizer(getWeatherResponse?.data);
    const city = cityNormalizer(getWeatherResponse?.data);

    yield put(weatherSliceActionTypes.getWeatherDataSuccess({ weatherData, city }));
  }
  if (getWeatherError) {
    yield put(
      weatherSliceActionTypes.getWeatherDataError(capitalize(getWeatherError.response.data.message))
    );
  }
}

function* getWeatherDataSaga() {
  yield all([takeLatest(weatherSliceActionTypes.getWeatherDataRequest, getWeatherData)]);
}

export default getWeatherDataSaga;
