import { createSelector } from '@reduxjs/toolkit';

const selectWeather = (state) => state.weather;

export const weatherDataSelector = createSelector(selectWeather, (weather) => weather.weatherData);

export const citySelector = createSelector(selectWeather, (weather) => weather.city);

export const isFetchingWeatherDataSelector = createSelector(
  selectWeather,
  (weather) => weather.isFetching
);

export const errorSelector = createSelector(selectWeather, (weather) => weather.error);
