import { capitalize } from 'utils';

export const cityNormalizer = (response) => response?.name;

export const weatherDataNormalizer = (response) => ({
  Description: capitalize(`${response?.weather[0]?.description}`),
  Temperature: `${(response?.main?.temp - 273.15).toFixed(1)} °C`,
  Humidity: `${response?.main?.humidity} %`,
  Pressure: `${Math.floor(response?.main?.pressure * 0.7501)} mm Hg`,
  Wind: `${response?.wind?.speed.toFixed(1)} m/s`,
});
