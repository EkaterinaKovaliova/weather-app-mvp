import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { ButtonComponent, Spinner } from './ButtonStyles';

function Button({ destruction, disabled, isFetching, onClickHandler, text, type, variant }) {
  return (
    <ButtonComponent
      $destruction={destruction}
      disabled={disabled}
      $isFetching={isFetching}
      onClick={onClickHandler}
      type={type}
      variant={variant}
    >
      {isFetching ? <Spinner /> : text}
    </ButtonComponent>
  );
}

Button.defaultProps = {
  destruction: false,
  disabled: false,
  isFetching: false,
  onClickHandler: () => {},
  type: 'submit',
  variant: 'contained',
};

Button.propTypes = {
  destruction: PropTypes.bool,
  disabled: PropTypes.bool,
  isFetching: PropTypes.bool,
  onClickHandler: PropTypes.func.isRequired,
  text: PropTypes.node.isRequired,
  type: PropTypes.string,
  variant: PropTypes.string,
};

export default memo(Button);
