import styled from 'styled-components';
import { Button, CircularProgress } from '@material-ui/core';

import { colors, spaces, typography } from 'styles';

export const ButtonComponent = styled(Button)`
  &.MuiButtonBase-root {
    width: 100%;
    height: 100%;
    padding: ${spaces.SPACE_10};
    border-radius: 8px;
    opacity: ${({ $disabled, $isFetching }) => ($disabled || $isFetching ? 0.5 : 1)};
    font-family: ${typography.font.TEXT};
    font-size: ${typography.sizes.SIZE_16};
    font-weight: 600;
    background-color: ${({ $destruction }) => ($destruction ? colors.MAIN_RED : colors.MAIN_GREEN)};
    color: ${colors.MAIN_WHITE};

    &:hover {
      background-color: ${({ $destruction }) =>
        $destruction ? colors.RED_HOVER : colors.GREEN_HOVER};
    }
  }
`;

export const Spinner = styled(CircularProgress).attrs({
  size: '20px',
})`
  &.MuiCircularProgress-root {
    color: ${colors.PRIMARY_DARK};
  }
`;
