import React from 'react';
import PropTypes from 'prop-types';

import { Paragraph } from './TextStyles';

function Text({ children, formDescription, formTitle, pageDescription, pageTitle }) {
  return (
    <Paragraph
      $pageTitle={pageTitle}
      $pageDescription={pageDescription}
      $formTitle={formTitle}
      $formDescription={formDescription}
    >
      {children}
    </Paragraph>
  );
}

Text.defaultProps = {
  pageTitle: false,
  pageDescription: false,
  formTitle: false,
  formDescription: false,
};

Text.propTypes = {
  pageTitle: PropTypes.bool,
  pageDescription: PropTypes.bool,
  formTitle: PropTypes.bool,
  formDescription: PropTypes.bool,
  children: PropTypes.node,
};

export default Text;
