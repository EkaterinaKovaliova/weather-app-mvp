import styled from 'styled-components';

import { colors, breakPoints, typography } from 'styles';

export const Paragraph = styled.p`
  margin: 0;
  overflow-wrap: anywhere;
  font-size: ${({ $pageTitle, $formDescription }) =>
    $pageTitle
      ? typography.sizes.SIZE_36
      : $formDescription
      ? typography.sizes.SIZE_16
      : typography.sizes.SIZE_20};
  font-family: ${({ $pageTitle, $formTitle }) =>
    $pageTitle || $formTitle ? typography.font.TITLE : typography.font.TEXT};
  color: ${({ $pageTitle, $pageDescription }) =>
    $pageTitle || $pageDescription ? colors.MAIN_WHITE : colors.PRIMARY_DARK};
  line-height: 1.5;

  @media (min-width: ${breakPoints.BREAKPOINT_600}) {
    font-size: ${({ $pageTitle, $formDescription }) =>
      $pageTitle
        ? typography.sizes.SIZE_60
        : $formDescription
        ? typography.sizes.SIZE_20
        : typography.sizes.SIZE_32};
  }
`;
