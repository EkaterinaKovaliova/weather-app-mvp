import React from 'react';
import PropTypes from 'prop-types';

import { ErrorField, InputWrapper, TextInput } from './TextFieldStyles';

function TextField({ error, label, name, onChange, value, variant }) {
  return (
    <InputWrapper>
      <TextInput
        $error={error}
        name={name}
        label={label}
        onChange={onChange}
        value={value}
        variant={variant}
      />
      <ErrorField>{error}</ErrorField>
    </InputWrapper>
  );
}

TextField.defaultProps = {
  variant: 'outlined',
};

TextField.propTypes = {
  error: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  variant: PropTypes.string,
};

export default TextField;
