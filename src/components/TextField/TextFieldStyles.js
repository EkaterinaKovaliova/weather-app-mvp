import styled from 'styled-components';
import { TextField } from '@material-ui/core';

import { colors, spaces, typography } from 'styles';

export const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const TextInput = styled(TextField)`
  &.MuiFormControl-root {
    width: 100%;
  }

  .MuiInputBase-root {
    border-radius: 7px;

    fieldset {
      border-width: 2px;
      border-color: ${({ $error }) => ($error ? colors.MAIN_RED : colors.PRIMARY_DARK)};
    }

    &.MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline {
      border-color: ${({ $error }) => ($error ? colors.MAIN_RED : colors.PRIMARY_DARK)};
    }
  }

  .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline {
    border-width: 3px;
    border-color: ${({ $error }) => ($error ? colors.MAIN_RED : colors.PRIMARY_DARK)};
  }

  &.MuiFormLabel-root {
    font-family: ${typography.font.TEXT};
    color: ${colors.GREY};
  }

  .MuiFormLabel-root.Mui-focused {
    color: ${colors.PRIMARY_DARK};
  }

  .MuiFormLabel-root.Mui-error {
    color: ${colors.GREY};
  }
`;

export const ErrorField = styled.div`
  min-height: ${spaces.SPACE_30};
  margin-top: ${spaces.SPACE_10};
  font-family: ${typography.font.TEXT};
  font-size: ${typography.sizes.SIZE_16};
  color: ${colors.MAIN_RED};
`;
