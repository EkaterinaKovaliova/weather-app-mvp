import axios from 'axios';

export const WEATHER_ID = '855ed4b256d36a44ed639405440ac80a';

const instance = axios.create({
  baseURL: 'http://api.openweathermap.org',
});

export default instance;
