import instance, { WEATHER_ID } from 'services/networkService';

export const getWeather = (city) => {
  return instance.get(`/data/2.5/weather?q=${city}&APPID=${WEATHER_ID}`);
};
