export const font = {
  TITLE: 'Lato-Bold',
  TEXT: 'Lato-Regular',
};

export const sizes = {
  SIZE_20: '20px',
  SIZE_24: '24px',
  SIZE_32: '32px',
  SIZE_36: '36px',
  SIZE_60: '60px',
};
