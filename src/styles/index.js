import { breakPoints } from './breakPoints';
import { colors } from './colors';
import { GlobalStyle } from './globalStyles';
import { spaces } from './spaces';
import * as typography from './typography';

export { breakPoints, colors, GlobalStyle, spaces, typography };
