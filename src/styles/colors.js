export const colors = {
  ADDITIONAL_BLUE: 'rgb(20,40, 70)',
  MAIN_RED: 'rgb(238, 68, 108)',
  RED_HOVER: 'rgb(252, 64, 64)',
  GREY: 'rgb(140, 158, 195)',
  MAIN_GREEN: 'rgb(24, 203, 132)',
  GREEN_HOVER: 'rgb(19, 218, 139)',
  MAIN_WHITE: 'rgb(255, 255, 255)',
  PRIMARY_DARK: 'rgb(26, 35, 53)',
};
