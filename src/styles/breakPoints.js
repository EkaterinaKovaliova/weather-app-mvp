export const breakPoints = {
  BREAKPOINT_600: '600px',
  BREAKPOINT_1024: '1024px',
};
