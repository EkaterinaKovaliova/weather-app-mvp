export const spaces = {
  SPACE_4: '4px',
  SPACE_10: '10px',
  SPACE_20: '20px',
  SPACE_30: '30px',
  SPACE_60: '60px',
  SPACE_100: '100px',
  SPACE_250: '250px',
};
