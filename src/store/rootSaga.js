import { all, fork } from 'redux-saga/effects';

import getWeatherDataSaga from 'modules/Weather/redux/saga';

export function* rootSaga() {
  yield all([fork(getWeatherDataSaga)]);
}
