import getWeatherReducer from 'modules/Weather/redux/weatherSlice';

export const rootReducer = {
  weather: getWeatherReducer,
};
