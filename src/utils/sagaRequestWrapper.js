import { call } from 'redux-saga/effects';

const requestWrapper = (saga, data) =>
  call(function* sagaRequest() {
    try {
      const response = yield call(saga, data);
      return { response };
    } catch (error) {
      return { error };
    }
  });
export default requestWrapper;
