import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './App';
import reportWebVitals from './reportWebVitals';
import configureAppStore from './store/configureStore';

import 'assets/fonts/Lato/index.css';
import { GlobalStyle } from 'styles';

const store = configureAppStore({});

ReactDOM.render(
  <Provider store={store}>
    <App />
    <GlobalStyle />
  </Provider>,
  document.getElementById('root')
);

reportWebVitals();
